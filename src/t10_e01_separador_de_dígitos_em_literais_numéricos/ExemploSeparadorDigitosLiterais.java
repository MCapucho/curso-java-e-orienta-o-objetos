package t10_e01_separador_de_d�gitos_em_literais_num�ricos;

public class ExemploSeparadorDigitosLiterais {

	public static void main(String[] args) {
		long populacaoSaoPaulo = 11_000_000L;
		long populcaoGrandeSaoPaulo = 11_000_000 + 9_000_000;
		
		System.out.printf("Popula��o S�o Paulo: %d\n", populacaoSaoPaulo);
		System.out.printf("Popula��o Grande S�o Paulo: %d\n", populcaoGrandeSaoPaulo);
		
		double precoVeiculo = 200_000.99_1d;
		System.out.printf("Pre�o Ve�culo: %.3f\n", precoVeiculo);
	
		int x = 1____2____3;
		int y = 1_2_3;
		System.out.printf("S�o iguais: %b\n", (x == y));
	}
	
}
