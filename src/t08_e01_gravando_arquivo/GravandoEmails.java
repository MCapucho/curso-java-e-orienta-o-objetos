package t08_e01_gravando_arquivo;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class GravandoEmails {

	public static void main(String[] args) {
		String[] emails = {"joao@joao.com", "maria@maria.com", "ricardo@ricardo.com"};
		
		BufferedWriter writer = null;
		
		try {
			writer = new BufferedWriter(new FileWriter("emails.txt", true));
			
			for(String email : emails) {
				writer.write(email);
				writer.newLine();
			}
		} catch(IOException e) {
			System.err.println("N�o conseguiu gravar o arquivo." + e.getMessage());
		} finally {
			try {
				writer.close();
			} catch (IOException e) {}
		}
	}
	
}
