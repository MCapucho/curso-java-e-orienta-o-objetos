package t05_e09_modificador_de_acesso_default.treinador;

import t05_e09_modificador_de_acesso_default.animal.Cachorro;

public class DonoCachorro {

	// Modificador default
	void ensinarCachorroSentar(Cachorro cachorro) {
		cachorro.sentar();
	}
}
