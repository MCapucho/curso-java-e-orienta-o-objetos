package t05_e09_modificador_de_acesso_default.animal;

public class Cachorro {

	private String nome;
	
	// Getters e Setters
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	// M�todo - Modificador default
	public void sentar() {
		System.out.println("Eu, " + nome + ", vou sentar!");
	}
}
