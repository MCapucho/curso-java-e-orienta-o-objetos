package t03_e12_passando_argumentos_para_m�todos;

public class Principal {

	public static void main(String[] args) {
		FolhaPagamento folhaPagamento = new FolhaPagamento();
		
		double salario = folhaPagamento.calcularSalario(160, 12, 32.50, 40.20);
		
		System.out.println("O sal�rio �: " + salario);
	}
}
