package t03_e12_passando_argumentos_para_m�todos;

public class FolhaPagamento {

	double calcularSalario(int horasNormais, int horasExtras, double valorHoraNormal, double valorHoraExtra) {
		double valorHorasNormais = horasNormais * valorHoraNormal;
		double valorHorasExtras = horasExtras * valorHoraExtra;
		
		return valorHorasNormais + valorHorasExtras;
	}
}
