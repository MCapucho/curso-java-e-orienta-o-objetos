package t02_e06_trabalhando_com_variáveis;

public class Variavel1 {

	public static void main(String[] args) {
		int quantidade; // Declarando variável inteiro
		quantidade = 10; // atribuindo o valor 10
		
		System.out.println(quantidade);
		
		quantidade = 15;
		
		System.out.println(quantidade);
	}
	
}
