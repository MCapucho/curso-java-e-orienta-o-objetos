package t05_e04_construtores;

public class Pessoa {

	String nome;
	int idade;
	
	Pessoa(String nome) {
		this.nome = nome;
	}
	
	Pessoa(String nome, int idade) {
		this(nome); // chama o primeiro construtor
		this.idade = idade;
	}
}
