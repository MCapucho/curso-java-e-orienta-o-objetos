package t05_e13_pacotes_e_enumeracoes;

public enum SituacaoConta {

	PENDENTE,
	PAGA,
	CANCELADA;
}
