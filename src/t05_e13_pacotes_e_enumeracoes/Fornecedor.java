package t05_e13_pacotes_e_enumeracoes;

public class Fornecedor {

	private String nome;
	
	// Construtor
	public Fornecedor() {
		
	}
	
	// Getters e Setters
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
}
