package t05_e21_classes_abstratas;

public abstract class Produto {
	
	protected String descricao;
	
	// Getters
	public String getDescricao() {
		return descricao;
	}

	public abstract void imprimirDescricao();
}
