package t05_e21_classes_abstratas;

public class ProdutoPerecivel extends Produto {
	
	protected String dataValidade;

	@Override
	public void imprimirDescricao() {
		System.out.println("Descri��o: " + getDescricao() + ".");
		System.out.println("Data de Validade: " + dataValidade + ".");
	}

}
