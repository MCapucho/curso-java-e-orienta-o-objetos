package t03_e06_instanciando_e_acessando_atributos_do_objeto;

public class PetShop {

	public static void main(String[] args) {
		Cachorro cachorro = new Cachorro();
		
		cachorro.nome = "Bidu";
		cachorro.idade = 1;
		cachorro.raca = "Boxer";
		cachorro.sexo = 'M';
		
		System.out.println("Cachorro");
		System.out.println("----------------------");
		System.out.println("Nome: " + cachorro.nome);
		System.out.println("Idade: " + cachorro.idade);
		System.out.println("Ra�a: " + cachorro.raca);
		System.out.println("Sexo: " + cachorro.sexo);
	}
}
