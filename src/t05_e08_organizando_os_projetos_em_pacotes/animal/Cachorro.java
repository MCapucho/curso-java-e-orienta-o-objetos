package t05_e08_organizando_os_projetos_em_pacotes.animal;

public class Cachorro {

	private String nome;
	
	// Getters e Setters
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
}
