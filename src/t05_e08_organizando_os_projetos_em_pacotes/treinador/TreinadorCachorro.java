package t05_e08_organizando_os_projetos_em_pacotes.treinador;

import t05_e08_organizando_os_projetos_em_pacotes.animal.Cachorro;

public class TreinadorCachorro {

	public static void main(String[] args) {
		Cachorro cachorro = new Cachorro();
		cachorro.setNome("Bob");
	}
}
