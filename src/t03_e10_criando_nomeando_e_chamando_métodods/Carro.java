package t03_e10_criando_nomeando_e_chamando_m�todods;

public class Carro {
	
	String fabricante; // inicia com null
	String modelo; // inicia com null
	String cor; // inicia com null
	int anoDeFabricacao; // inicia com 0
	boolean biCombustivel; // inicia com false

	Proprietario dono; // inicia com null
	
	void ligar() {
		System.out.println("Ligando o carro: " + modelo);
	}
	
}
