package t11_e08_API_de_data_parte_4.model;

import java.time.Duration;
import java.time.LocalDateTime;

public class Aluguel {

	private Cliente cliente;
	private Carro carro;
	
	private LocalDateTime dataHoraRetirada;
	private LocalDateTime dataHoraPrevista;
	private LocalDateTime dataHoraDevolucao;
	
	// Getters e Setters
	public Cliente getCliente() {
		return cliente;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Carro getCarro() {
		return carro;
	}
	
	public void setCarro(Carro carro) {
		this.carro = carro;
	}
	
	public LocalDateTime getDataHoraRetirada() {
		return dataHoraRetirada;
	}
	
	public void setDataHoraRetirada(LocalDateTime dataHoraRetirada) {
		this.dataHoraRetirada = dataHoraRetirada;
	}
	
	public LocalDateTime getDataHoraPrevista() {
		return dataHoraPrevista;
	}
	
	public void setDataHoraPrevista(LocalDateTime dataHoraPrevista) {
		this.dataHoraPrevista = dataHoraPrevista;
	}
	
	public LocalDateTime getDataHoraDevolucao() {
		return dataHoraDevolucao;
	}
	
	public void setDataHoraDevolucao(LocalDateTime dataHoraDevolucao) {
		this.dataHoraDevolucao = dataHoraDevolucao;
	}
	
	// Construtor
	public Aluguel(Cliente cliente, Carro carro, 
			       LocalDateTime dataHoraRetirada, 
			       LocalDateTime dataHoraPrevista) {
		this.cliente = cliente;
		this.carro = carro;
		this.dataHoraRetirada = dataHoraRetirada;
		this.dataHoraPrevista = dataHoraPrevista;
	}

	// M�todo
	public void imprimirFatura() {
		Duration duration = Duration.between(dataHoraPrevista, dataHoraDevolucao);
		
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>FATURA<<<<<<<<<<<<<<<<<<<<<<<<<");
		if (!duration.isNegative()) {
			double multa = duration.toHours() * carro.getValorDiaria() * 0.1;

			System.out.printf("Valor da multa: R$ %.2f. Pois voc� atrasou %s dias.", multa, duration.toHours());
		} else {
			System.out.println("Obrigado. Volte sempre");
		}
		
	}
	
}
