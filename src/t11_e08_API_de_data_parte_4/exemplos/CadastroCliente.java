package t11_e08_API_de_data_parte_4.exemplos;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;

import t11_e08_API_de_data_parte_4.model.Cliente;

public class CadastroCliente {

	public static void main(String[] args) {
		Cliente cliente = new Cliente("Jo�o Silva", LocalDate.of(1988, Month.AUGUST, 13));
		
		int idade = Period.between(cliente.getDataNascimento(), LocalDate.now()).getYears();
		
		if (idade >= 18) {
			System.out.println("OK, pode ser cadastrado. Sua idade �: " + idade);
		} else {
			System.out.printf("Sua idade �: %d. N�o pode ter carteira.", idade);
		}
	}
	
}
