package t11_e08_API_de_data_parte_4.exemplos;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Year;

import t11_e08_API_de_data_parte_4.model.Aluguel;
import t11_e08_API_de_data_parte_4.model.Carro;
import t11_e08_API_de_data_parte_4.model.Cliente;

public class DevolucaoCarro {

	public static void main(String[] args) {
		Aluguel aluguel = recuperarAluguel();
		
		LocalDateTime dataHoraDevolucao = LocalDateTime.of(LocalDate.now().plusDays(5), LocalTime.now());
		aluguel.setDataHoraDevolucao(dataHoraDevolucao);
		aluguel.imprimirFatura();
	}
	
	private static Aluguel recuperarAluguel() {
		Cliente cliente = new Cliente("Jo�o Silva", LocalDate.of(1985, Month.FEBRUARY, 8));
		Carro carro = new Carro("Gol", 90.0, Year.of(2012));
		
		LocalDateTime dataHoraRetirada = LocalDateTime.of(LocalDate.now(), LocalTime.now());
		LocalDateTime dataHoraPrevista = LocalDateTime.now().plusDays(3);
		Aluguel aluguel = new Aluguel(cliente, carro, dataHoraRetirada, dataHoraPrevista);
		
		return aluguel;
	}
}
