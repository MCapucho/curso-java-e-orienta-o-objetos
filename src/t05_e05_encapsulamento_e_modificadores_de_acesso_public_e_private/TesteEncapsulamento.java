package t05_e05_encapsulamento_e_modificadores_de_acesso_public_e_private;

public class TesteEncapsulamento {

	public static void main(String[] args) {
		ArCondicionado ar = new ArCondicionado(); // 17� - 25�
		
		ar.trocarTemperatura(21);
		System.out.println("Temperatura do AR: " + ar.obterTemperatura() + "�");
		
		ar.trocarTemperatura(10);
		System.out.println("Temperatura do AR: " + ar.obterTemperatura() + "�");
	}
}
