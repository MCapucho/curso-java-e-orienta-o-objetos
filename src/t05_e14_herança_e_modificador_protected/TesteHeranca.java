package t05_e14_heran�a_e_modificador_protected;

public class TesteHeranca {

	public static void main(String[] args) {
		Jogador jogador = new Jogador();
		jogador.nome = "Ronaldo";
		jogador.idade = 33;
		
		System.out.println("O nome do jogador � " + jogador.nome + " e tem " + jogador.idade + " anos.");
		
		jogador.apresentar();
		jogador.dizerSeAindaJoga();
	}
}
