package t05_e14_heran�a_e_modificador_protected;

public class Pessoa {

	protected String nome;
	protected int idade;
	
	// Getters e Setters
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public int getIdade() {
		return idade;
	}
	
	public void setIdade(int idade) {
		this.idade = idade;
	}
	
	// Construtor
	public Pessoa() {
	
	}
	
	// M�todo
	public void apresentar() {
		System.out.println("Ol�, eu sou " + nome + ", e tenho " + idade + " anos.");
	}
}
