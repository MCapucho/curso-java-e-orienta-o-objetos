package t11_e01_introdução_ao_lambda.util;

public class EnviadorEmail {

	public void  enviar(String email, String texto) {
		System.out.println("Enviando o email para " + email + ". Com texto: " + texto);
	}
}
