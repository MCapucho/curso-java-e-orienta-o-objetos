package t11_e01_introdu��o_ao_lambda.model;

public class Fatura {

	private String emailDevedor;
	private double valor;
	private boolean emailEnviado;
	
	// Getters e Setters
	public String getEmailDevedor() {
		return emailDevedor;
	}
	
	public void setEmailDevedor(String emailDevedor) {
		this.emailDevedor = emailDevedor;
	}
	
	public double getValor() {
		return valor;
	}
	
	public void setValor(double valor) {
		this.valor = valor;
	}
		
	public boolean isEmailEnviado() {
		return emailEnviado;
	}

	public void setEmailEnviado(boolean emailEnviado) {
		this.emailEnviado = emailEnviado;
	}

	// Construtor
	public Fatura(String emailDevedor, double valor) {
		this.emailDevedor = emailDevedor;
		this.valor = valor;
	}
	
	// M�todo
	public String resumo() {
		return "Valor devido: " + this.valor;
	}
}
