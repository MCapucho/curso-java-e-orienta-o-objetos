package t11_e01_introdução_ao_lambda.exemplo;

import java.util.List;

import t11_e01_introdução_ao_lambda.dao.FaturaDAO;
import t11_e01_introdução_ao_lambda.model.Fatura;
import t11_e01_introdução_ao_lambda.util.EnviadorEmail;

public class ExemploLambda {

	public static void main(String[] args) {
		List<Fatura> faturasVencidas = new FaturaDAO().buscarFaturasVencidas();
		
		EnviadorEmail enviadorEmail = new EnviadorEmail();
		
		/*for (Fatura f : faturasVencidas) {
		 *	enviadorEmail.enviar(f.getEmailDevedor(), f.resumo());
		 *}
		 */
		
		faturasVencidas.forEach(f ->  {
			enviadorEmail.enviar(f.getEmailDevedor(), f.resumo());
			f.setEmailEnviado(true);
		});
		
	}
}
