package t03_e11_m�todos_com_retorno;

public class Principal {

	public static void main(String[] args) {
		Paciente paciente = new Paciente();
		
		paciente.peso = 78.8;
		paciente.altura = 1.74;
		
		IMC imc = paciente.calcularIndiceDeMassaCorporal();
		
		System.out.println("Abaixo do Peso: " + imc.abaixoPeso);
		System.out.println("Peso Ideal: " + imc.pesoIdeal);
		System.out.println("Obeso: " + imc.obeso);
		System.out.println("Grau de Obesidade: " + imc.grauObesidade);
	}
}
