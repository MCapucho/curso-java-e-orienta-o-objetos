package t05_e20_polimorfismo_casting_de_objetos_e_instanceof;

public class Conta {

	protected double saldo;
	
	// Getters e Setters
	public double getSaldo() {
		return saldo;
	}
	
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
}
