package t05_e20_polimorfismo_casting_de_objetos_e_instanceof;

public class ContaCorrente extends Conta {

	private double limite = 1000;
	
	// Getters
	public double getLimite() {
		return limite;
	}
}
