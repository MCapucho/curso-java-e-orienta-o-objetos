package t05_e20_polimorfismo_casting_de_objetos_e_instanceof;

public class ContaPoupanca extends Conta {

	private double rendimentos = 30;
	
	// Getters
	public double getRendimentos() {
		return rendimentos;
	}
}
