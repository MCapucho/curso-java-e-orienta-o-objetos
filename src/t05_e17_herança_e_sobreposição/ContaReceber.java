package t05_e17_heran�a_e_sobreposi��o;

public class ContaReceber extends Conta {
	
	private Cliente cliente;
	
	// Getters e Setters
	public Cliente getCliente() {
		return cliente;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	// Construtor
	public ContaReceber() {
		
	}
	
	public ContaReceber(Cliente cliente, String descricao, Double valor, String dataVencimento) {
		this.cliente = cliente;
		this.setDescricao(descricao);
		this.setValor(valor);
		this.setDataVencimento(dataVencimento);
	}
	
	public void receber() {
		if (SituacaoConta.PAGA.equals(this.getSituacaoConta())) {
			System.out.println("N�o se pode receber uma conta que j� est� paga: " + this.getDescricao());
		} else if (SituacaoConta.CANCELADA.equals(this.getSituacaoConta())) {
			System.out.println("N�o se pode receber uma conta que j� est� cancelada: " + this.getDescricao());
		} else {
			System.out.println("Conta recebida: " + this.getDescricao()
			                  + " no valor de " + this.getValor()
			                  + " na data de vencimento: " + this.getDataVencimento()
			                  + " do cliente: " + this.getCliente().getNome());
			
			this.situacaoConta = SituacaoConta.PAGA;
		}
	}
	
	public void cancelar() {
		if (this.getValor() > 50000d) {
			System.out.println("Essa conta � receber n�o pode ser cancelada. " 
		                      + "� muito dinheiro para deixar de receber: " 
		                      + this.getDescricao());
		} else {
			super.cancelar();
		}
	}
}
