package t03_e14_composi��o_de_objetos_e_chamada_de_m�todos;

public class Passeio {

	public static void main(String[] args) {
		Pessoa pessoa = new Pessoa();
		pessoa.nome = "Murylo";
		
		pessoa.cachorro = new Cachorro();
		pessoa.cachorro.nome = "Thor";
		
		Caminhada caminhada = new Caminhada();
		caminhada.andar(pessoa);
	}
}
