package t03_e07_composi��o_de_objetos;

public class Principal {

	public static void main(String[] args) {
		Proprietario proprietario = new Proprietario();
		
		proprietario.nome = "Jo�o da Silva";
		proprietario.cpf = "000.000.000-00";
		proprietario.idade = 25;
		proprietario.logradouro = "Rua Jo�o Pinheiro, 10";
		proprietario.bairro = "Centro";
		proprietario.cidade = "Uberl�ndia";
		
		Carro meuCarro = new Carro();
		
		meuCarro.fabricante = "Fiat";
		meuCarro.modelo = "Palio";
		meuCarro.cor = "Prata";
		meuCarro.anoDeFabricacao = 2011;
		meuCarro.dono = proprietario;		
		
		System.out.println("Meu Carro");
		System.out.println("----------------------");
		System.out.println("Fabricante: " + meuCarro.fabricante);
		System.out.println("Modelo: " + meuCarro.modelo);
		System.out.println("Cor: " + meuCarro.cor);
		System.out.println("Ano: " + meuCarro.anoDeFabricacao);
		System.out.println("Dono: " + meuCarro.dono);
		
	}
}
