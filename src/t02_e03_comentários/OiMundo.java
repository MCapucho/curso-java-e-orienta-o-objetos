package t02_e03_coment�rios;

public class OiMundo {
	
	/*
	 * Esta linha ser� ignorada pelo compilador
	 * System.out.println("essa instru��o ser� ignorada);
	 */
	
	// A linha abaixo envia uma mensagem para o monitor
	public static void main(String[] args) {
		System.out.println("Oi Mundo");
	}
	
}
