package t03_e04_instanciando_objetos;

public class Principal {

	public static void main(String[] args) {
		Carro meuCarro = new Carro();
		Carro seuCarro = new Carro();
		
		System.out.println(meuCarro);
		System.out.println(seuCarro);
	}
}
