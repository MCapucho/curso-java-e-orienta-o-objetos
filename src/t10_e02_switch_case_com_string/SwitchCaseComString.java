package t10_e02_switch_case_com_string;

public class SwitchCaseComString {

	public static void main(String[] args) {
		String carro = "Ferrari";
		
		/*
		 * if (carro.equals("Audi")) { System.out.println("Alem�o"); } 
		 * else if (carro.equals("Ferrari")) { System.out.println("Italiano"); }
		 */
		
		switch (carro) {
		case "Audi":
			System.out.println("Alem�o");
			break;
		case "Ferrari":
			System.out.println("Italiano");
			break;
		case "Honda":
			System.out.println("Japon�s");
			break;
		default:
			System.out.println("N�o informou nenhum carro!");
		}
	}
}
