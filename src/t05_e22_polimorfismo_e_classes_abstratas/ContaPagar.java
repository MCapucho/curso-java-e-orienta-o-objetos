package t05_e22_polimorfismo_e_classes_abstratas;

public class ContaPagar extends Conta{

	private Fornecedor fornecedor;
	
	// Construtor	
	public ContaPagar() {
		
	}
	
	public ContaPagar(Fornecedor fornecedor, String descricao, Double valor, String dataVencimento) {
		this.fornecedor = fornecedor;
		this.setDescricao(descricao);
		this.setValor(valor);
		this.setDataVencimento(dataVencimento);
	}
	
	// Getters e Setters
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	// M�todo
	public void exibirDetalhes() {
		System.out.println("\nConta a Pagar");
		System.out.println("===================================");
		System.out.println("Fornecedor: " + this.getFornecedor().getNome());
		System.out.println("Descri��o: " + this.getDescricao());
		System.out.println("Valor: " + this.getValor());
		System.out.println("Data de vencimento: " + this.getDataVencimento());
		System.out.println("Situa��o: " + this.getSituacaoConta());
		System.out.println("===================================");
	}
	
	public void pagar() {
		if (SituacaoConta.PAGA.equals(this.getSituacaoConta())) {
			System.out.println("N�o pode pagar uma conta que j� est� paga: " + this.getDescricao() + ".");
		} else if (SituacaoConta.CANCELADA.equals(this.getSituacaoConta())) {
			System.out.println("N�o pode pagar uma conta que j� est� cancelada: " + this.getDescricao() + ".");
		} else {
			System.out.println("Pagando conta " + this.getDescricao() 
			                  + " no valor de " + this.getValor() 
			                  + " e vencimento em " + this.getDataVencimento()
			                  + " do fornecedor " + this.getFornecedor().getNome() + ".");
			
			this.situacaoConta = SituacaoConta.PAGA;
		}
	}
	
}
