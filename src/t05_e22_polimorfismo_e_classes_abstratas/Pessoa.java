package t05_e22_polimorfismo_e_classes_abstratas;

public abstract class Pessoa {

	protected String nome;
	
	// Getters e Setters
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
}
