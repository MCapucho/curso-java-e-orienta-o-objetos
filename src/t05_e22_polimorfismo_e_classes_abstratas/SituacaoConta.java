package t05_e22_polimorfismo_e_classes_abstratas;

public enum SituacaoConta {

	PENDENTE,
	PAGA,
	CANCELADA;
}
