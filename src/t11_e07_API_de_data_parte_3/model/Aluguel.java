package t11_e07_API_de_data_parte_3.model;

import java.time.LocalDateTime;

public class Aluguel {

	private Cliente cliente;
	private Carro carro;
	
	private LocalDateTime dataHoraRetirada;
	private LocalDateTime dataHoraPrevista;
	
	// Getters e Setters
	public Cliente getCliente() {
		return cliente;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Carro getCarro() {
		return carro;
	}
	
	public void setCarro(Carro carro) {
		this.carro = carro;
	}
	
	public LocalDateTime getDataHoraRetirada() {
		return dataHoraRetirada;
	}
	
	public void setDataHoraRetirada(LocalDateTime dataHoraRetirada) {
		this.dataHoraRetirada = dataHoraRetirada;
	}
	
	public LocalDateTime getDataHoraPrevista() {
		return dataHoraPrevista;
	}
	
	public void setDataHoraPrevista(LocalDateTime dataHoraPrevista) {
		this.dataHoraPrevista = dataHoraPrevista;
	}
	
	// Construtor
	public Aluguel(Cliente cliente, Carro carro, 
			       LocalDateTime dataHoraRetirada, 
			       LocalDateTime dataHoraPrevista) {
		this.cliente = cliente;
		this.carro = carro;
		this.dataHoraRetirada = dataHoraRetirada;
		this.dataHoraPrevista = dataHoraPrevista;
	}
	
}
