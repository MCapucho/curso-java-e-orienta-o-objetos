package t11_e07_API_de_data_parte_3.exemplos;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Year;
import java.time.format.DateTimeFormatter;

import t11_e07_API_de_data_parte_3.model.Aluguel;
import t11_e07_API_de_data_parte_3.model.Carro;
import t11_e07_API_de_data_parte_3.model.Cliente;

public class NovoAluguel {

	public static void main(String[] args) {
		Cliente cliente = new Cliente("Jo�o Silva", LocalDate.of(1985, Month.FEBRUARY, 8));
		Carro carro = new Carro("Gol", 90.0, Year.of(2012));
		
		LocalDateTime dataHoraRetirada = LocalDateTime.of(LocalDate.now(), LocalTime.now());
		LocalDateTime dataHoraPrevista = LocalDateTime.now().plusDays(3);
		Aluguel aluguel = new Aluguel(cliente, carro, dataHoraRetirada, dataHoraPrevista);
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
		
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>RECIBO<<<<<<<<<<<<<<<<<<<<<<<<<");
		System.out.println("Carro: " + aluguel.getCarro().getModelo());
		System.out.println("Cliente: " + aluguel.getCliente().getNome());
		System.out.println("Data e Hora da retirada: " + aluguel.getDataHoraRetirada().format(formatter));
		System.out.println("Data e Hora da previs�o para devolu��o: " + aluguel.getDataHoraPrevista().format(formatter));
	}
	
}
