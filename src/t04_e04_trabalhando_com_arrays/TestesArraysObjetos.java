package t04_e04_trabalhando_com_arrays;

public class TestesArraysObjetos {

	public static void main(String[] args) {
		Carro[] carros = new Carro[4];
		
		carros[0] = new Carro();
		carros[0].anoDeFabricacao = 2011;
		
		System.out.println("Ano de fabricação: " + carros[0].anoDeFabricacao);
	}
}
