package t03_e09_vari�veis_referenciam_objetos;

public class Principal {

	public static void main(String[] args) {
		Proprietario proprietario = new Proprietario();
		proprietario.nome = "Jo�o da Silva";
		
		Carro meuCarro = new Carro();
		meuCarro.modelo = "Palio";
		
		meuCarro.dono = proprietario;
		
		System.out.println("meuCarro.dono.nome: " + meuCarro.dono.nome);
		System.out.println("Proprietario: " + proprietario.nome);
	}
}
