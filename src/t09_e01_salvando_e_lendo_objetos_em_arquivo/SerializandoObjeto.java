package t09_e01_salvando_e_lendo_objetos_em_arquivo;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

public class SerializandoObjeto {

	public static void main(String[] args) {
		Pessoa p = new Pessoa();
		p.setNome("Jo�o Silva");
		p.setIdade(25);
		p.setProfissao("Motorista");
		
		try (ObjectOutput output = new ObjectOutputStream(new FileOutputStream("joao.obj"))) {
			output.writeObject(p);
			System.out.println("Objeto salvo com sucesso.");
		} catch (IOException e) {
			System.err.println("Erro salvando o objeto. Erro: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
}
