package t05_e18_sobrecarga;

public class TesteCadastro {

	public static void main(String[] args) {
		CadastroPessoa cadastroPessoa = new CadastroPessoa();
		
		Pessoa pessoa = new Pessoa("Jos�", 32);
		cadastroPessoa.cadastrar(pessoa);
		
		cadastroPessoa.cadastrar("Maria", 23);
		
		cadastroPessoa.cadastrar("Pedro");
	}
}
