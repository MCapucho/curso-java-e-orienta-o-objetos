package t03_e08_valores_padr�o;

public class Principal {

	public static void main(String[] args) {
		Carro seuCarro = new Carro();
		
		seuCarro.anoDeFabricacao = 2009;
		seuCarro.cor = "Preto";
		
		seuCarro.dono.nome = "Jos� Pereira";
		
		System.out.println("Seu Carro");
		System.out.println("----------------------");
		System.out.println("Cor: " + seuCarro.cor);
		System.out.println("Ano: " + seuCarro.anoDeFabricacao);
		System.out.println("Dono: " + seuCarro.dono);
		
	}
}
