package t05_e10_modificadores_static_e_final.modificadorstatic;

public class Contador {

	public static int COUNT = 0; // Letra mai�scula -> static
	
	public static final double PI = 3.14; // Letra mai�scula -> constante = n�o muda o valor
	
	// M�todo
	public void incrementar() {
		COUNT++;
	}
	
	public static void imprimirContador() {
		System.out.println("O valor do contador agora �: " + Contador.COUNT);
	}
}
