package t05_e10_modificadores_static_e_final.modificadorstatic;

public class TesteContador {

	@SuppressWarnings("static-access")
	public static void main(String[] args) {
		Contador contador = new Contador();
		contador.incrementar();
		
		System.out.println(Contador.COUNT); // Modo correto
		
		Contador.COUNT++;
		
		System.out.println(contador.COUNT); // N�o recomendafo fazer desse jeito	
		
		Contador.imprimirContador();
	}
}
