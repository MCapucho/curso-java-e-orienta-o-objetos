package t10_e04_try_with_resources_e_multi_catch;

public class NegocioException extends RuntimeException {

	public NegocioException(String msg) {
		super(msg);
	}

}