package t11_e02_refer�ncia_a_m�todos.exemplo;

import java.util.List;

import t11_e02_refer�ncia_a_m�todos.dao.FaturaDAO;
import t11_e02_refer�ncia_a_m�todos.model.Fatura;

public class Exemplo {

	public static void main(String[] args) {
		List<Fatura> faturasVencidas = new FaturaDAO().buscarFaturasVencidas();
		
		/*for (Fatura f : faturasVencidas) {
		 *	f.atualizarStatus();
		 *}
		 */
		
		/*faturasVencidas.forEach(f -> {
		 *	f.atualizarStatus();
		 *});
		 */
		
		faturasVencidas.forEach(Fatura::atualizarStatus);
	}
	
}
