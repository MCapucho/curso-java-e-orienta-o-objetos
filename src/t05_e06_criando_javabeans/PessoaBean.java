package t05_e06_criando_javabeans;

public class PessoaBean {

	private String nome;
	private int idade;
	
	// Construtor
	public PessoaBean() {
		
	}
	
	// Getters e Setters
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public int getIdade() {
		return idade;
	}
	
	public void setIdade(int idade) {
		this.idade = idade;
	}
}
