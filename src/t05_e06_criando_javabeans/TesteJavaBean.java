package t05_e06_criando_javabeans;

public class TesteJavaBean {

	public static void main(String[] args) {
		PessoaBean pessoaBean = new PessoaBean();
		pessoaBean.setNome("Maria");
		pessoaBean.setIdade(27);
		
		System.out.println(pessoaBean.getNome() + " tem " + pessoaBean.getIdade() + " anos.");
	}

}
