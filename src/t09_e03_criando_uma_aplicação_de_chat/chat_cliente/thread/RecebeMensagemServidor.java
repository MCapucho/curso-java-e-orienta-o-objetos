package t09_e03_criando_uma_aplica��o_de_chat.chat_cliente.thread;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.InputStream;
import java.net.Socket;

import t09_e03_criando_uma_aplica��o_de_chat.chat_cliente.gui.JanelaGui;


public class RecebeMensagemServidor implements Runnable {

	private Socket socket;
	private JanelaGui janela;
	
	public RecebeMensagemServidor(Socket socket, JanelaGui janela) {
		this.socket = socket;
		this.janela = janela;
	}
	
	@Override
	public void run() {
		
		while (true) {
			try {
				InputStream entrada = this.socket.getInputStream();
				DataInput dataInput = new DataInputStream(entrada);
				
				String msgRecebida = dataInput.readUTF();
				
				janela.adicionaMensagem(msgRecebida);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
