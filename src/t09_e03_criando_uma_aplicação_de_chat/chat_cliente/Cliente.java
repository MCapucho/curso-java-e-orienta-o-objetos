package t09_e03_criando_uma_aplica��o_de_chat.chat_cliente;

import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.OutputStream;
import java.net.Socket;

import t09_e03_criando_uma_aplica��o_de_chat.chat_cliente.gui.JanelaGui;
import t09_e03_criando_uma_aplica��o_de_chat.chat_cliente.thread.RecebeMensagemServidor;

public class Cliente extends JanelaGui {
	
	private Socket socket;

	public static void main(String[] args) {
		new Cliente();
	}

	@Override
	protected boolean conectar() {
		System.out.println("Conectando no servidor...");
		
		try {
			
			this.socket = new Socket("127.0.0.1", 3333);
			
			RecebeMensagemServidor recebeMensagemServidor = new RecebeMensagemServidor(this.socket, this);
			new Thread(recebeMensagemServidor).start();
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return false;
		
		}
		
		return true;
	}

	@Override
	protected void sendMessage(String mensagem) {
		System.out.println("Envia a mensagem via socket para o servidor - " + mensagem);
		
		try {
			
			OutputStream saida = this.socket.getOutputStream();
			DataOutput dataOutput = new DataOutputStream(saida);
			dataOutput.writeUTF(mensagem);
			
		} catch (Exception e) {
			
			e.printStackTrace();
		
		}
	}
}
