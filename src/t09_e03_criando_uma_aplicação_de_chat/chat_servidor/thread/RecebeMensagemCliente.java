package t09_e03_criando_uma_aplica��o_de_chat.chat_servidor.thread;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.OutputStream;
import java.net.Socket;

import t09_e03_criando_uma_aplica��o_de_chat.chat_servidor.Servidor;

public class RecebeMensagemCliente implements Runnable {

	private Socket socket;
	private Servidor servidor;
		
	public RecebeMensagemCliente(Socket socket, Servidor servidor) {
		this.socket = socket;
		this.servidor = servidor;
	}

	@Override
	public void run() {
		
		while (true) {
			System.out.println("Aguardando mensagem do cliente...");
			
			try {
			
				 DataInput dataInput = new DataInputStream(this.socket.getInputStream());
				 String msgRecebida = dataInput.readUTF();
				 this.servidor.enviarMensagensClientes(msgRecebida);
				
			} catch (EOFException e) {
				
				System.out.println("Cliente desconectado");
				break;
			
			} catch (Exception e) {
				
				e.printStackTrace();
				
			}
		}
	}

	public void enviarMensagem(String mensagem) {
		try {
			
			OutputStream saida = this.socket.getOutputStream();
			DataOutput dataOutput = new DataOutputStream(saida);
			dataOutput.writeUTF(mensagem);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
		}
	}
}
