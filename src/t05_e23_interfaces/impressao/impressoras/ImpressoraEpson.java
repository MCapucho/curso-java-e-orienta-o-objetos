package t05_e23_interfaces.impressao.impressoras;

import t05_e23_interfaces.impressao.Impressora;
import t05_e23_interfaces.impressao.Imprimivel;

public class ImpressoraEpson implements Impressora {

	@Override
	public void imprimir(Imprimivel imprimivel) {
		System.out.println("********************************");
		System.out.println(imprimivel.getCabecalhoPagina());
		System.out.println("********************************");
		System.out.println(imprimivel.getCorpoPagina());
		System.out.println();
		System.out.println("--------------------------------");
		System.out.println("==           EPSON            ==");
		System.out.println("--------------------------------");
	}

}
