package t05_e23_interfaces.impressao;

public interface Imprimivel {

	public String getCabecalhoPagina();
	public String getCorpoPagina();
	
}
