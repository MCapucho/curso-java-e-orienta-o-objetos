package t05_e23_interfaces.impressao;

public interface Impressora {

	public abstract void imprimir(Imprimivel imprimivel);
}
