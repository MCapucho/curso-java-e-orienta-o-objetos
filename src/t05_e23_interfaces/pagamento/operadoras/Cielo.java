package t05_e23_interfaces.pagamento.operadoras;

import t05_e23_interfaces.pagamento.Autorizavel;
import t05_e23_interfaces.pagamento.Cartao;
import t05_e23_interfaces.pagamento.Operadora;

public class Cielo implements Operadora {

	@Override
	public boolean autorizar(Autorizavel autorizavel, Cartao cartao) {
		return cartao.getNumeroCartao().startsWith("123") &&
				autorizavel.getValorTotal() < 100;
	}

}
