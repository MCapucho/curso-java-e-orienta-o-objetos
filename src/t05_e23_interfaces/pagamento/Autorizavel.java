package t05_e23_interfaces.pagamento;

public interface Autorizavel {

	public double getValorTotal();
}
