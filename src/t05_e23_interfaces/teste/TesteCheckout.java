package t05_e23_interfaces.teste;

import t05_e23_interfaces.caixa.Checkout;
import t05_e23_interfaces.caixa.Compra;
import t05_e23_interfaces.impressao.Impressora;
import t05_e23_interfaces.impressao.impressoras.ImpressoraEpson;
import t05_e23_interfaces.pagamento.Cartao;
import t05_e23_interfaces.pagamento.Operadora;
import t05_e23_interfaces.pagamento.operadoras.Cielo;

public class TesteCheckout {

	public static void main(String[] args) {
		Operadora operadora = new Cielo();
		Impressora impressora = new ImpressoraEpson();
		
		Cartao cartao = new Cartao();
		cartao.setNomeTitular("Jo�o M Couves");
		cartao.setNumeroCartao("123");
		
		Compra compra = new Compra();
		compra.setNomeCliente("Jo�o Mendon�a Couves");
		compra.setProduto("Sabonete");
		compra.setValorTotal(2.5);
		
		Checkout checkout = new Checkout(operadora, impressora);
		checkout.fecharCompra(compra, cartao);
	}
}
