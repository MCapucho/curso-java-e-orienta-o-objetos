package t05_e07_objeto_this_construtores_e_javaBeans;

public class Fornecedor {

	private String nome;
	
	// Construtor
	public Fornecedor() {
		
	}
	
	// Getters e Setters
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
}
