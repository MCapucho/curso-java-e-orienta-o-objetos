package t03_e13_argumentos_por_valor_ou_referencia;

public class Produto {

	void  definirPreco(Preco preco, double percentualImpostos, double margemLuro) {
		preco.valorImpostos = preco.valorCustos * (percentualImpostos / 100);
		preco.valorLucro = preco.valorCustos * (margemLuro / 100);
		preco.precoVenda = preco.valorCustos + preco.valorImpostos + preco.valorLucro;
	}
}
