package t02_e04_sequências_de_escape;

public class OiMundo {

	public static void main(String[] args) {
		System.out.println("Oi \"Mundo\"");
		System.out.println("Oi \\Mundo\\");
		System.out.println("Oi \nMundo");
	}
	
}
