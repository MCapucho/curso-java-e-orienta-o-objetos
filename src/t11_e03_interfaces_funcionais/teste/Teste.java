package t11_e03_interfaces_funcionais.teste;

import t11_e03_interfaces_funcionais.compra.Compra;
import t11_e03_interfaces_funcionais.impressao.Impressora;

public class Teste {

	public static void main(String[] args) {
		// Impressora i = new ImpressoraHP();
		
		// Sem par�metro
		/*Impressora i = () -> {
		 *	System.out.println("Simulando a impress�o.");
		 *};
		 */
		
		// Com par�metro
		Impressora i = (compra) -> {
			System.out.println("Simulando a impress�o. Seria impresso: " + compra);
		};
		
		Compra compra = new Compra("sabonete", 2.5);
		i.imprimir(compra);
	}
}
