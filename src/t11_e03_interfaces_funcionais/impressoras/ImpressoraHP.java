package t11_e03_interfaces_funcionais.impressoras;

import t11_e03_interfaces_funcionais.compra.Compra;
import t11_e03_interfaces_funcionais.impressao.Impressora;

public class ImpressoraHP implements Impressora {

	@Override
	public void imprimir(Compra compra) {
		System.out.println("Enviando o comando para a impressora HP -> " + compra);
	}

}
