package t11_e03_interfaces_funcionais.impressao;

import t11_e03_interfaces_funcionais.compra.Compra;

@FunctionalInterface
public interface Impressora {

	public void imprimir(Compra compra);
}
