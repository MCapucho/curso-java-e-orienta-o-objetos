package t11_e03_interfaces_funcionais.compra;

public class Compra {

	private String produto;
	private double valor;
	
	// Getters e Setters
	public String getProduto() {
		return produto;
	}
	
	public void setProduto(String produto) {
		this.produto = produto;
	}
	
	public double getValor() {
		return valor;
	}
	
	public void setValor(double valor) {
		this.valor = valor;
	}
	
	// Construtor
	public Compra(String produto, double valor) {
		super();
		this.produto = produto;
		this.valor = valor;
	}
	
	@Override
	public String toString() {
		return "Produto: " + this.produto + ". Valor: R$ " + this.valor;
	}
}
