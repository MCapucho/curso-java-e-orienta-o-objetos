package t11_e04_introdução_a_stream.teste;

import java.util.List;

import t11_e04_introdução_a_stream.dao.FaturaDAO;
import t11_e04_introdução_a_stream.model.Fatura;

public class Teste {

	public static void main(String[] args) {
		
		List<Fatura> faturas = new FaturaDAO().buscarTodasFaturas();
		
		/*for (Fatura f : faturas) {
		 *	if (f.getValor() > 250.0) {
		 *		System.out.println("Alerta: Fatura acima de R$ 250,00 -> " + f);
		 *	}
		 *}
		 */
		
		faturas.stream()
			   .filter(Fatura :: estaEmRisco)
		       .forEach(System.out::println);
	}
}
