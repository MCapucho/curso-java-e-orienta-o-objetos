package t11_e04_introdu��o_a_stream.model;

public class Fatura {

	private String email;
	private double valor;
	
	// Getters e Setters
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public double getValor() {
		return valor;
	}
	
	public void setValor(double valor) {
		this.valor = valor;
	}
	
	// Construtor
	public Fatura(String email, double valor) {
		super();
		this.email = email;
		this.valor = valor;
	}
	
	// M�todo
	public boolean estaEmRisco() {
		return valor >= 250 ? true : false;
	}
	
	@Override
	public String toString() {
		return "Email: " + getEmail() + ". Valor R$ " + getValor();
	}
}
