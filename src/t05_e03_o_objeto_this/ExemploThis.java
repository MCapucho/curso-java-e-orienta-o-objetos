package t05_e03_o_objeto_this;

public class ExemploThis {

	public static void main(String[] args) {
		Carro carro = new Carro();
		carro.modelo = "Palio";
		
		System.out.println("Modelo Normal: " + carro.modelo);
		System.out.println("------------------------------");
		
		carro.alterarModelo("Civic");
		System.out.println("Modelo Alterado: " + carro.modelo);
	}
}
