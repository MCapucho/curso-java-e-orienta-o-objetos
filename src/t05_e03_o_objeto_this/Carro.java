package t05_e03_o_objeto_this;

public class Carro {
	
	String fabricante; // inicia com null
	String modelo; // inicia com null
	String cor; // inicia com null
	int anoDeFabricacao; // inicia com 0
	boolean biCombustivel; // inicia com false

	Proprietario dono; // inicia com null
	
	void alterarModelo(String modelo) {
		if (modelo != null) {
			this.modelo = modelo;
		}
	}
	
	void ligar() {
		System.out.println("Ligando o carro: " + modelo);
	}
	
}
