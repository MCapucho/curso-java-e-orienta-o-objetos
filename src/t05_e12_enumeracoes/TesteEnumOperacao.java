package t05_e12_enumeracoes;

public class TesteEnumOperacao {

	public static void main(String[] args) {
		OperacaoAritmetica op = OperacaoAritmetica.ADICAO;
		int resultado = op.operacao(12, 5);
		
		System.out.println("O resultado �: " + resultado);
		
		for (OperacaoAritmetica oa: OperacaoAritmetica.values()) {
			System.out.println(oa + " -> " + oa.operacao(4, 2));
		}
	}
}
