package t05_e12_enumeracoes;

public enum Naipe {

	OURO("Vermelho"),
	PAUS("Preto"),
	ESPADA("Preto"),
	COPA("Vermelho");
	
	private String cor;
	
	// Construtor
	Naipe(String cor) {
		this.cor = cor;
	}
		
	// Getters
	public String getCor() {
		return cor;
	}
}
