package t05_e12_enumeracoes;

public class Carta {

	private int numero;
	private Naipe naipe;
	
	// Construtor
	public Carta(int numero, Naipe naipe) {
		this.numero = numero;
		this.naipe = naipe;
	}
	
	// M�todo
	public void imprimirCarta() {
		System.out.println("A carta �: " + numero + " de " + naipe + ". " + "A cor da carta �: " + naipe.getCor());
	}
}
